package mk.ukim.finki.wp.videoaggregator.service;

import mk.ukim.finki.wp.videoaggregator.model.Category;

import java.util.Optional;

public interface CategoryService {

  Optional<Category> findOne(Long categoryId);

  Category save(Category category);

  Iterable<Category> findAll();

  Iterable<Category> findByTitle(String title);

  Iterable<Category> findByTitleLike(String title);

  void delete(Long id);

}
