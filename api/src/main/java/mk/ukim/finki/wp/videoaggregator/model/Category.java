package mk.ukim.finki.wp.videoaggregator.model;

import org.hibernate.annotations.Where;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Riste Stojanov
 */
@Indexed
@Entity
@Table(name = "categories")
@Where(clause = "deleted=false")
@Cacheable
public class Category implements Serializable {

  @Id
  @GeneratedValue
  public Long id;

  @Field(index = Index.YES, store = Store.NO, analyze = Analyze.YES)
  @Analyzer(definition = "emtAnalyser")
  @Boost(2f)
  public String title;

  public boolean deleted = false;

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Category category = (Category) o;

    return id != null ? id.equals(category.id) : category.id == null;
  }
}
