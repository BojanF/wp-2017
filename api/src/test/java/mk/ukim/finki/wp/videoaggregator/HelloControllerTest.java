package mk.ukim.finki.wp.videoaggregator;

import com.jayway.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@ActiveProfiles({"test", "jpa"})
public class HelloControllerTest extends BaseIntegrationTest {

    @Test
    public void test_get_hello_controller() {
        given().accept(ContentType.HTML)
                .when()
                .log().all()
                .get("/hello_controller")
                .then()
                .log().all()
                .assertThat()
                .contentType(ContentType.HTML)
                .statusCode(HttpStatus.SC_OK)
                .body(is(not(isEmptyOrNullString())));
    }

    @Test
    public void test_get_hello_to_default() {
        given().accept(ContentType.HTML)
                .when()
                .log().all()
                .get("/hello_to")
                .then()
                .log().all()
                .assertThat()
                .contentType(ContentType.HTML)
                .statusCode(HttpStatus.SC_OK)
                .body(containsString("Riste"));
    }
}
